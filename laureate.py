import webapp2
import  os
import urllib
import wsgiref.handlers

from google.appengine.ext.webapp import template


class MainPage(webapp2.RequestHandler):
    def get(self):
        #self.response.headers['Content-Type'] = 'text/plain'
        #self.response.write('!Hola, Mundo!')
        templates = {}
        path = os.path.join(os.path.dirname(__file__), 'index.html')
        self.response.out.write(template.render(path, templates))
 
app = webapp2.WSGIApplication([
    ('/', MainPage),
], debug=True)

def main():
	app.run()

if __name__ == '__main__':
	main()